package pl.edu.pjwstk.gdansk.s10908.asd.sort;

import java.util.Arrays;

public class Sort {

	private static final void swap(int[] a, int i, int j) {
		int t = a[i];
		a[i] = a[j];
		a[j] = t;
	}

	public static int[] bubbleSort(int[] tab) {
		int counter;
		for (int k = 0; k < tab.length; k++) {
			counter = 0;
			for (int i = 1; i < tab.length - k; i++) {
				if (tab[i] < tab[i - 1]) {
					swap(tab, i, i - 1);
					counter++;
				}
			}
			if (counter == 0)
				break;
		}

		return tab;
	}

	public static int[] insertSort(int[] tab) {
		if (tab.length > 1) {
			int temp = 0;
			int i = 0;
			for (int j = 1; j < tab.length; j++) {
				temp = tab[j];
				i = j - 1;
				while (i >= 0 && tab[i] > temp) {
					tab[i + 1] = tab[i];
					tab[i] = temp;
					i--;
				}
			}
		}
		return tab;
	}

	public static void heapify(int[] a, int first, int last) {
		int largest = first;
		while ((largest * 2 + 1) <= last) { 
			int left = largest * 2 + 1; 
			if (left + 1 <= last && a[left] < a[left + 1]) {
				left++; 
			}
			if (a[largest] < a[left]) {
				swap(a, largest, left);
				largest = left; 
			} else
				return;
		}
	}

	public static void heapBuild(int[] a, int length) {
		int start = (length - 2) / 2;

		while (start >= 0) {
			heapify(a, start, length - 1);
			start--;
		}
	}

	public static int[] heapSort(int[] a) {
		int length = a.length;

		heapBuild(a, length);

		int end = length - 1;
		while (end > 0) {
			swap(a, 0, end);
			heapify(a, 0, end - 1);
			end--;
		}
		return a;
	}

	private static int[] merge(final int[] tab1, final int[] tab2) {
		int sortedTab[] = new int[tab1.length + tab2.length];
		int i = 0;
		int p = 0;
		int q = 0;

		while (p < tab1.length && q < tab2.length) {
			sortedTab[i++] = (tab1[p] < tab2[q]) ? tab1[p++] : tab2[q++];
		}
		while (p < tab1.length) {
			sortedTab[i++] = tab1[p++];
		}
		while (q < tab2.length) {
			sortedTab[i++] = tab2[q++];
		}
		return sortedTab;
	}

	public static int[] mergeSort(int tab[]) {
		if (tab.length > 1) {
			int[] left = Arrays.copyOfRange(tab, 0, tab.length / 2);
			int[] right = Arrays.copyOfRange(tab, left.length, tab.length);

			tab = merge(mergeSort(left), mergeSort(right));
		}
		return tab;
	}

	private static int partitionH(int[] tab, int left, int right) {
		final int s = tab[left];

		while (left <= right) {
			while (tab[left] < s) {
				left++;
			}
			while (tab[right] > s) {
				right--;
			}

			if (left <= right) {
				swap(tab, left, right);
				left++;
				right--;
			}
		}
		return left;
	}

	private static int partitionL(int[] tab, final int first, final int last) {
		int x = tab[last];
		int i = first - 1;

		for (int j = first; j <= last - 1; j++) {
			if (tab[j] <= x) {
				swap(tab, ++i, j);
			}
		}
		swap(tab, ++i, last);
		return i;
	}

	private static int[] quickSortL(int[] tab, final int first, final int last) {
		if (first < last) {
			final int s = partitionL(tab, first, last);
			tab = quickSortL(tab, first, s - 1);
			tab = quickSortL(tab, s, last);
		}

		return tab;
	}

	private static int[] quickSortH(int[] tab, final int first, final int last) {

		final int s = partitionH(tab, first, last);
		if (first < s - 1) {
			tab = quickSortH(tab, first, s - 1);
		}

		if (last > s) {
			tab = quickSortH(tab, s, last);
		}
		return tab;
	}

	public static int[] quickSortL(int[] tab) {
		return quickSortL(tab, 0, tab.length - 1);
	}

	public static int[] quickSortH(int[] tab) {
		return quickSortH(tab, 0, tab.length - 1);
	}

	public static boolean isSortedInc(int tab[]) {
		for (int i = 0; i < tab.length - 1; i++) {
			if (tab[i] > tab[i + 1]) {
				return false;
			}
		}
		return true;
	}

	public static boolean isSortedDec(int tab[]) {
		for (int i = 0; i < tab.length - 1; i++) {
			if (tab[i] < tab[i + 1]) {
				return false;
			}
		}
		return true;
	}
}
