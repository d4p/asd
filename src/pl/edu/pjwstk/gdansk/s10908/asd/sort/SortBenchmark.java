package pl.edu.pjwstk.gdansk.s10908.asd.sort;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class SortBenchmark {
	private int testArrayLength;
	private int[] testTab;
	private long startTime;
	private long stopTime;
	private static int[] sortedTab;
	//
	private Random random;
	private Set<Integer> buffor;
	private int randomNumber;
	private int bufforSize;

	public SortBenchmark() {
		random = new Random();
		buffor = new HashSet<Integer>();
	}

	public SortBenchmark(int testArrayLength) {
		this();
		this.testArrayLength = testArrayLength;
		initRandom();
	}

	public void initRandom(int testArrayLength) {
		setTestArrayLength(testArrayLength);
		initRandom();
	}

	private void initRandom() {
		testTab = new int[testArrayLength];
		buffor.clear();
		for (int j = 0; buffor.size() < testArrayLength;) {
			bufforSize = buffor.size();
			randomNumber = random.nextInt();
			buffor.add(randomNumber);
			if (bufforSize < buffor.size()) {
				testTab[j] = randomNumber;
				j++;
			}
		}
	}

	public void initSortedInc(int testArrayLength) {
		setTestArrayLength(testArrayLength);
		initSortedInc();
	}

	private void initSortedInc() {
		testTab = new int[testArrayLength];
		for (int j = 0; j < testArrayLength; j++) {
			testTab[j] = j;
		}
	}

	public void initSortedDec(int testArrayLength) {
		setTestArrayLength(testArrayLength);
		initSortedDec();
	}

	private void initSortedDec() {
		testTab = new int[testArrayLength];
		for (int j = 0; j < testTab.length; j++) {
			testTab[j] = testTab.length - j;
		}
	}

	public void printTestArray() {
		System.out.println("\nTest array: \n" + Arrays.toString(testTab));
	}

	public void printSortedArray() {
		System.out.println("Sorted array: \n" + Arrays.toString(sortedTab)
				+ "\n\n");
	}

	private String testSort(Algorithm algorithm) {
		int[] testCopy;
		sortedTab = new int[testTab.length];
		switch (algorithm) {
		case MERGE_SORT:
			testCopy = Arrays.copyOf(testTab, testTab.length);
			startTime = System.currentTimeMillis();
			sortedTab = Sort.mergeSort(testCopy);
			stopTime = System.currentTimeMillis();
			break;
		case BUBBLE_SORT:
			testCopy = Arrays.copyOf(testTab, testTab.length);
			startTime = System.currentTimeMillis();
			sortedTab = Sort.bubbleSort(testCopy);
			stopTime = System.currentTimeMillis();
			break;
		case HEAP_SORT:
			testCopy = Arrays.copyOf(testTab, testTab.length);
			startTime = System.currentTimeMillis();
			sortedTab = Sort.heapSort(testCopy);
			stopTime = System.currentTimeMillis();
			break;
		case INSERT_SORT:
			testCopy = Arrays.copyOf(testTab, testTab.length);
			startTime = System.currentTimeMillis();
			sortedTab = Sort.insertSort(testCopy);
			stopTime = System.currentTimeMillis();
			break;
		case QUICK_SORT_L:
			testCopy = Arrays.copyOf(testTab, testTab.length);
			startTime = System.currentTimeMillis();
			sortedTab = Sort.quickSortL(testCopy);
			stopTime = System.currentTimeMillis();
			break;

		case QUICK_SORT_H:
			testCopy = Arrays.copyOf(testTab, testTab.length);
			startTime = System.currentTimeMillis();
			sortedTab = Sort.quickSortH(testCopy);
			stopTime = System.currentTimeMillis();
			break;
		default:
			return "ERROR - nieobslugiwany algorytm";
		}

		if (Sort.isSortedInc(sortedTab)) {
			return /* algorithm.name() + "," + */"" + (stopTime - startTime);
		} else {
			return "ERROR (" + algorithm.name()
					+ "): tablica nie zostala prawidlowo posortowana.";
		}
	}

	public int getTestArrayLength() {
		return testArrayLength;
	}

	public void setTestArrayLength(int testArrayLength) {
		this.testArrayLength = testArrayLength;
	}

	enum Algorithm {
		MERGE_SORT, BUBBLE_SORT, INSERT_SORT, HEAP_SORT, QUICK_SORT_H, QUICK_SORT_L;
	}

	public static void main(String[] args) {
		SortBenchmark benchmarkRandom = new SortBenchmark();
		SortBenchmark benchmarkInc = new SortBenchmark();
		SortBenchmark benchmarkDec = new SortBenchmark();
		int[] testArrayLength = { 100000, 250000 };
		System.out.println("PROBA,ALGORYTM,ROZMIAR_TAB,PORZADEK,CZAS(MS),PORZADEK,CZAS(MS),PORZADEK,CZAS(MS)");
		for (int i : testArrayLength) {
			for (int k = 1; k <= 10; k++) {
				benchmarkRandom.initRandom(i);
				benchmarkInc.initSortedInc(i);
				benchmarkDec.initSortedDec(i);
				for (Algorithm algorithm : Algorithm.values()) {
					System.out
							.print(k + "," + algorithm.name() + "," + i + ",");
					System.out.print("LOSOWY,"
							+ benchmarkRandom.testSort(algorithm) + ",");
					System.out.print("ROSNACY,"
							+ benchmarkInc.testSort(algorithm) + ",");
					System.out.print("MALEJACY,"
							+ benchmarkDec.testSort(algorithm) + "\n");
				}
			}
		}
	}
}