package pl.edu.pjwstk.gdansk.s10908.asd.code.huffmann;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HuffmanCode {
	private Map<String, Integer> countingMap;
	private Map<String, String> keyCodeMap;
	private File sourceFile;
	private File destinationFile;

	private final Charset CHARSET = Charset.forName("UTF-8");

	public HuffmanCode(String sourceDir, String destinationDir) {
		sourceFile = new File(sourceDir);
		destinationFile = new File(destinationDir);
		init();
	}

	private void init() {
		countingMap = new HashMap<String, Integer>();
		keyCodeMap = new HashMap<String, String>();

	}

	public void decrypt() throws IOException, ClassNotFoundException {
		init();
		loadKeyFromFile();
		keyCodeMap = reverse(keyCodeMap);
		decryptToFile();
	}

	public String fillTo8BitString(String bitString) {
		while (bitString.length() < 8) {
			bitString = "0" + bitString;
		}
		return bitString;
	}

	public void decryptToFile() throws IOException {
		BufferedReader buffReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(sourceFile), CHARSET));
		PrintWriter writer = new PrintWriter(destinationFile, CHARSET.name());
		String decodeBuffor = "";
		String keyCode;
		long fileLength = sourceFile.length();
		for (int i = 0; i < fileLength - 1; i++) {
			if (i < fileLength - 2) {
				decodeBuffor += fillTo8BitString(Integer
						.toBinaryString(buffReader.read()));
			} else {
				String lastByte = Integer.toBinaryString(buffReader.read());
				int controlByte = buffReader.read();
				for (int n = 0; n < 8 - lastByte.length() - controlByte; n++) {
					lastByte = "0" + lastByte;
				}
				decodeBuffor += lastByte;
			}
			for (int k = 0; k < decodeBuffor.length(); k++) {
				keyCode = keyCodeMap.get(decodeBuffor.substring(0, k));
				if (keyCode != null) {
					decodeBuffor = decodeBuffor.substring(k);
					writer.print(keyCode);
					break;
				}
			}
		}
		writer.close();
		buffReader.close();
	}

	public static <K, V> HashMap<V, K> reverse(Map<K, V> map) {
		HashMap<V, K> rev = new HashMap<V, K>();
		for (Map.Entry<K, V> entry : map.entrySet())
			rev.put(entry.getValue(), entry.getKey());
		return rev;
	}

	public void encrypt() throws IOException {
		init();
		generateKeyCodeMap();

		while (countingMap.size() > 1) {
			connectMinKeys();
		}
		
		saveKeyCodeMapToFile();
		saveBytesToFile(prepareByteList(textToBinaryString()));
	}
	
	private void generateKeyCodeMap() throws IOException {
		BufferedReader buffReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(sourceFile), CHARSET));

		String currentChar;
		for (int i = 0; i < sourceFile.length(); i++) {
			currentChar = String.valueOf((char) buffReader.read());
			if (countingMap.containsKey(currentChar)) {
				countingMap.put(currentChar, countingMap.get(currentChar)
						.intValue() + 1);
			} else {
				countingMap.put(currentChar, 1);
			}
			keyCodeMap.put(currentChar, "");
		}
		buffReader.close();
	}
	
	private String textToBinaryString() throws IOException {
		BufferedReader buffReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(sourceFile), CHARSET));
		String binaryString = "";
		for (int i = 0; i < sourceFile.length(); i++) {
			binaryString += keyCodeMap.get(String.valueOf((char) buffReader
					.read()));
		}
		buffReader.close();
		return binaryString;
	}
	
	private List<String> prepareByteList(String binaryString) {
		List<String> byteList = new ArrayList<String>(
				Arrays.asList(binaryString.split("(?<=\\G.{8})")));
		String controlByte = "00000000";
		String lastByte = byteList.get(byteList.size() - 1);
		if (lastByte.length() < 8) {
			byteList.remove(byteList.size() - 1);
			int i = 0;
			while (i < 8 - lastByte.length()) {
				lastByte = "0" + lastByte;
				i++;
			}
			String iB = Integer.toBinaryString(i);
			byteList.add(lastByte);
			controlByte = controlByte.substring(0, 8 - iB.length()) + iB;
		}
		byteList.add(controlByte);
		return byteList;
	}

	private void saveKeyCodeMapToFile() throws IOException {
		File keyFile = new File(destinationFile.getPath() + ".key");
		FileOutputStream fileOutputStream = new FileOutputStream(keyFile);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(
				fileOutputStream);
		objectOutputStream.writeObject(keyCodeMap);
		objectOutputStream.close();
	}

	private void saveBytesToFile(List<String> byteList) throws IOException {
		PrintWriter writer = new PrintWriter(destinationFile, CHARSET.name());
		for (String line : byteList) {
			writer.print((char) Integer.parseInt(line, 2));
		}
		writer.close();
	}

	@SuppressWarnings("unchecked")
	public void loadKeyFromFile() throws ClassNotFoundException, IOException {
		File keyFile = new File(sourceFile.getPath() + ".key");
		FileInputStream fileInputStream = new FileInputStream(keyFile);
		ObjectInputStream objectInputStreamp = new ObjectInputStream(
				fileInputStream);
		keyCodeMap = (HashMap<String, String>) objectInputStreamp.readObject();
		objectInputStreamp.close();
	}

	private String getMinKey() {
		String minKey = null;
		Integer minValue = Integer.MAX_VALUE;
		for (Map.Entry<String, Integer> entry : countingMap.entrySet()) {
			if (entry.getValue() < minValue) {
				minKey = entry.getKey();
				minValue = entry.getValue();
			}
		}
		return minKey;
	}

	private void connectMinKeys() {
		String leftMinKey = getMinKey();
		int leftMinValue = countingMap.remove(leftMinKey);

		String rightMinKey = getMinKey();
		int rightMinValue = countingMap.remove(rightMinKey);

		for (char sign : leftMinKey.toCharArray()) {
			String signString = String.valueOf(sign);
			keyCodeMap.put(signString, "0" + keyCodeMap.get(signString));
		}

		for (char sign : rightMinKey.toCharArray()) {
			String signString = String.valueOf(sign);
			keyCodeMap.put(signString, "1" + keyCodeMap.get(signString));
		}
		countingMap.put(leftMinKey + rightMinKey, leftMinValue + rightMinValue);
	}

	public static void main(String[] args) {
		HuffmanCode encryptCodeTool = new HuffmanCode("fileToEncrypt.txt",
				"encrypted.txt");
		HuffmanCode decryptCodeTool = new HuffmanCode("encrypted.txt",
				"decrypted.txt");
		try {
			encryptCodeTool.encrypt();
			decryptCodeTool.decrypt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
